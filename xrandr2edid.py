#!/usr/bin/python

'''
Usage: ./xrandr2edid.py

Saves EDID from xrandr output. Run and read the printed message.

The command 'xrandr --verbose' prints EDID for each display.
This script parses it, and save them to the files 'edid<n>.bin', where n is a number.
'''

# Author: Teika Kazura <teika@gmx.com>
# License: MIT License
# Website: https://gitlab.com/teika-gentoo/xrandr2edid

import subprocess, re, enum

class searchFor(enum.Enum):
    display = 1
    edidHeader = 2
    EDID = 3 # In fact, the above 2 are dummy.

def parseXRandROutput():
    """Parses the output of 'xrandr --verbose', and save each EDID."""
    output = subprocess.run(['xrandr', '--verbose'],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                            encoding='utf-8').stdout

    mode = searchFor.display
    nthEdid = 0

    for line in output.splitlines():
        if re.match(r'^\w+', line):
            print('* ' + line)
            mode = searchFor.edidHeader
            continue
        if 'EDID:' in line:
            match = re.match(r'(\s)+EDID:', line)
            if match:
                # Assumes that lines with 16 octets, following the line 'EDID:', form an EDID.
                pat = re.compile(r'\s+([0-9a-f]{32})')
                mode = searchFor.EDID
                edid = ''
                nthEdid += 1
                continue

        if mode == searchFor.EDID:
            match = pat.match(line)
            if match:
                edid += match.group(1)
            else:
                print('-->   Saved edid{}.bin'.format(nthEdid))
                with open('edid{}.bin'.format(nthEdid), 'wb') as f:
                    f.write(bytearray.fromhex(edid))
                mode = searchFor.display

if __name__ == '__main__':
    parseXRandROutput()
